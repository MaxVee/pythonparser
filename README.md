# Пример парсера 101 формы банка с сайта Центрального банка (https://www.cbr.ru)
Данный [Python](/Python/101CB.py) скрипт "парсит" таблицу(101 форма) с сайта Центрального банка РФ для заданного банка(**regnum**) и на дату (**dt**) и записывает данные в **.xlsx** файл.  Используемые инструменты: requests, BeautifulSoup.

Пример URL: 
 https://www.cbr.ru/banking_sector/credit/coinfo/f101/?regnum=2268&dt=2020-01-01    

 См. в папке [Python](/Python/) фйал **101CB.py**
