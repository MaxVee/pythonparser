"""

@author: (c) MAFedorov<fedorov.maxim.al@gmail.com>

"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

import urllib

import os

import requests
from bs4 import BeautifulSoup

import xlsxwriter


# Генерируем URL прокси, если необходим 
def setProxies(login, pwd, url, port):
        
    proxies = {}
    
    proxies['https'] = 'https://'+login+':' + \
        urllib.parse.quote(pwd) + '@' + url + ':' + port 

    proxies['http'] = 'http://' + login + ':' + \
        urllib.parse.quote(pwd) + '@' + url + ':' + port

    return proxies


# ЦБ 101 форма
def CB_Form(regnum, dt, login, pwd, proxy=False):
    
    if proxy:
        proxies = setProxies(login, pwd)

    else: proxies=''
    
    r = requests.get('https://www.cbr.ru/banking_sector/credit/coinfo/f101/?regnum=' + \
                      regnum + '&dt=' + dt, proxies=proxies)
    
    r.encoding = 'utf-8'

    tbl = BeautifulSoup(r.text, "html")('table')[0]

    rows = []
    
    for i, tr in enumerate(tbl.find_all('tr')[6:]):
        
        tds=tr.find_all('td')
        
        if len(tds) < 13: continue

        row = []
        
        for j, td in enumerate(tds):
    
            if j == 0:
                row.append(td.text)
    
            else:
                row.append(float(td.text.encode("latin-1").decode('utf-8', 'ignore')))
    
        rows.append(row)

    return rows

# Пишем в Excel
def xlsx_write(rows, open_xlsx=True):

    print("Creating xlsx file...")

    workbook = xlsxwriter.Workbook('forma101.xlsx')

    worksheet = workbook.add_worksheet()

    columns = ["Balance account", "2", "3", "4", "5",
               "6", "7", "8", "9", "10", "11", "12", "13"]

    for i, col in enumerate(columns):
        worksheet.write(0, i, col)

    for i, row in enumerate(rows):
        for j, col in enumerate(row):
            worksheet.write(i + 1, j, col)

    workbook.close()
    
    if open_xlsx:
        from sys import platform
        
        if platform == "linux" or platform == "linux2":
            os.system('soffice --calc forma101.xlsx')  
        
        elif platform == "win32":
            os.system('start excel.exe forma101.xlsx')

    print("Ready!")


if __name__ == '__main__':

    # пароль и логин для proxy
    login = ''

    pwd = ''

    # регистрационный номер банка и дата(формат даты - в обратном порядке)
    regnum = '2268'

    dt = '2020-01-01'

    data101 = CB_Form(regnum, dt, login, pwd)

    xlsx_write(data101)
